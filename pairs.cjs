const testObject = require('./objects.cjs');

function pairs(object) {
    if(typeof object !== 'object'|| !object)
    return [];
    let pairsArray = [];
    for(let key in object){
        pairsArray.push([key,object[key]]);
    }
    return pairsArray;
}

module.exports = pairs;