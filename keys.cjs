const testObject = require('./objects.cjs');
let newObj=testObject.testObject1;

function keys(object) {
    if(typeof object !== 'object'|| !object)
    return [];
    let keyArray=[];
    for(const key in object){
        keyArray.push(key);
    }
    return keyArray
}
module.exports = keys;