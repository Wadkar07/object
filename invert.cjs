let testObject = require('./objects.cjs');
testObject=testObject.testObject2;
function invert(object){
    if(typeof object !== 'object'|| !object)
    return {};
    let newObj ={};
    for(let key in object){
        newObj[object[key]] = key;
    }
    return newObj;
}
module.exports = invert;