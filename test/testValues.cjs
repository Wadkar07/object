let testObject = require('../objects.cjs')
testObject = testObject.testObject;
const values = require('../values.cjs')
var expect = require('chai').expect;

describe('Values function',()=>{
    it('Returns array of values of an object',()=>{
        expect(values(testObject)).to.eql([ 'Bruce Wayne', 36, 'Gotham' ]);
    })
    it('Returns empty array',()=>{
        expect(values()).to.eql([]);
    })
    it('Returns empty array',()=>{
        expect(values(2)).to.eql([]);
    })
})
