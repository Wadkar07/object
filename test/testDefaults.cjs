let testObject = require('../objects.cjs')
let newObj=testObject.testObject3;
const defaults = require('../defaults.cjs')
var expect = require('chai').expect;

describe('Invert function',()=>{
    it('Returns an object',()=>{
        expect(defaults(newObj,{'key1':'value1'})).to.eql({ name: 'Bruce Wayne', age: 36, location: 'Gotham', key1: 'value1' });
    })
    it('Returns empty object',()=>{
        expect(defaults()).to.eql({});
    })
    it('Returns empty object',()=>{
        expect(defaults(2)).to.eql({});
    })
})


