let testObject = require('../objects.cjs')
let newObj=testObject.testObject1;
const mapObject = require('../mapObject.cjs')
var expect = require('chai').expect;

describe('MapObject function',()=>{
    it('Returns array of keys of an object',()=>{
        expect(mapObject(newObj)).to.eql({ name: 'Bruce Wayne3', age: 39, location: 'Gotham3' });
    })
    it('Returns empty array',()=>{
        expect(mapObject()).to.eql({});
    })
    it('Returns empty array',()=>{
        expect(mapObject(2)).to.eql({});
    })
})
