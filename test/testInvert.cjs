let testObject = require('../objects.cjs')
let newObj=testObject.testObject2;
const invert = require('../invert.cjs')
var expect = require('chai').expect;

describe('Invert function',()=>{
    it('Returns array of keys of an object',()=>{
        expect(invert(newObj)).to.eql({ '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' });
    })
    it('Returns empty array',()=>{
        expect(invert()).to.eql({});
    })
    it('Returns empty array',()=>{
        expect(invert(2)).to.eql({});
    })
})

