let testObject = require('../objects.cjs')
testObject = testObject.testObject;
const pairs = require('../pairs.cjs')
var expect = require('chai').expect;

describe('Pairs function',()=>{
    it('Returns array of keys nad values pairs of an object',()=>{
        expect(pairs(testObject)).to.eql([ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]);
    })
    it('Returns empty array',()=>{
        expect(pairs()).to.eql([]);
    })
    it('Returns empty array',()=>{
        expect(pairs(2)).to.eql([]);
    })
})
