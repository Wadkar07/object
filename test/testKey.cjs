let testObject = require('../objects.cjs')
testObject = testObject.testObject;
const key = require('../key.cjs')
var expect = require('chai').expect;

describe('Keys function',()=>{
    it('Returns array of keys of an object',()=>{
        expect(key(testObject)).to.eql([ 'name', 'age', 'location' ]);
    })
    it('Returns empty array',()=>{
        expect(key()).to.eql([]);
    })
    it('Returns empty array',()=>{
        expect(key(2)).to.eql([]);
    })
})