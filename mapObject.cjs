let testObject = require('./objects.cjs');
testObject=testObject.testObject1;

const keys = require('./keys.cjs')

function mapObject(object) {
    if(typeof object !== 'object'|| !object)
    return {};
    keyArray = keys(object);

    keyArray.forEach(function (key) {
        var value = object[key];
        object[key] = value + 3;
    });
    return object;   
}
// console.log(mapObject(testObject));
module.exports = mapObject;