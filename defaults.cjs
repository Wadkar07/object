// let testObject = require('./objects.cjs');
// testObject=testObject.testObject3
function defaults(obj, defaultProps) {
    if(typeof obj !== 'object'|| !obj)
    return {};
    if(typeof defaultProps !== 'object'||!defaultProps)
    return obj;
    for(const key in defaultProps){
        if(!(key in obj)||obj[key] === undefined){
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}
// newObj ={'key1':'value1'};
// console.log(defaults(testObject,newObj));
module.exports = defaults;
