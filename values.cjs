const testObject = require('./objects.cjs');

function values(object) {
    if(typeof object !== 'object'|| !object)
    return [];
    let valuesArray = [];
    for(let key in object){
        valuesArray.push(object[key]);
    }
    return valuesArray;
}

module.exports= values;